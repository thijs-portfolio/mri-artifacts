In this project I investigated the formation of MRI image distortion artifacts surrounding biliary stents. With a controlled experimental setup I determined magnetic field distortions and T_2 dephasing effects around several different biliary stents. This knowledge can help correct for artifacts around biliary stents in sensitive MRI acquisitions in an in vivo situation.
This could greatly reduce radiation treatment planning inaccuracies in pancreatic cancer patients.

<object data="https://gitlab.com/thijsbs-portfoio/mri-artifacts/raw/master/MRI%20biliary%20stent%20artifacts.pdf?inline=false" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/thijsbs-portfoio/mri-artifacts/raw/master/MRI%20biliary%20stent%20artifacts.pdf?inline=false">
        <p>To view please <a href="https://gitlab.com/thijsbs-portfoio/mri-artifacts/raw/master/MRI%20biliary%20stent%20artifacts.pdf?inline=false">download the PDF</a>. Or preview it in gitlab</p>
    </embed>
</object>

